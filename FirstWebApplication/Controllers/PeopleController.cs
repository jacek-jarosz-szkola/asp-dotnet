﻿using FirstWebApplication.Database.Context;
using FirstWebApplication.Database.Entities;
using FirstWebApplication.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace FirstWebApplication.Controllers
{
    public class PeopleController : Controller
    {
        private PeopleDbContext context = new PeopleDbContext();


        public IActionResult Index()
        {
            return View();
        }


        public IActionResult CreatePerson()
        {
            return View();
        }


        [HttpPost]
        public IActionResult CreatePerson(PersonDTO personData)
        {
            context.Persons.Add(new Person { Age=personData.Age, Name=personData.Name, Surname=personData.Surname});
            context.SaveChanges();
            return Redirect("/People/ShowAllPersons");
        }


        public IActionResult ViewPerson()
        {
            var person = new PersonDTO { Name = "Jacek", Surname = "Pracek", Age = 18 };
            return View(person);
        }

        public IActionResult ShowAllPersons()
        {
            List<PersonDTO> model = context.Persons.Select(x => new PersonDTO() { Id = x.Id, Age = x.Age, Name = x.Name, Surname = x.Surname }).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult EditPerson(PersonDTO data)
        {

            Person person = context.Persons.FirstOrDefault(x => x.Id == data.Id);

            if (person == null)
                return NotFound();

            person.Name = data.Name;
            person.Surname = data.Surname;
            person.Age = data.Age;

            context.SaveChanges();
            
            return RedirectToAction("ShowAllPersons", "People");    
        }


        public IActionResult EditPerson([FromRoute] int id)
        {
            var person = context.Persons.Where(x => x.Id == id)
                .Select(x =>
                new PersonDTO { 
                    Id = x.Id,
                    Age = x.Age,
                    Name = x.Name,
                    Surname = x.Surname
                })
                .FirstOrDefault();
            return View(person);
        }


        public IActionResult DeletePerson([FromRoute] int id)
        {
            var person = context.Persons.FirstOrDefault(x => x.Id == id);
            
            if (person == null) return NotFound();
            
            context.Persons.Remove(person);
            context.SaveChanges();
            return RedirectToAction("ShowAllPersons");
        }
    }
}
