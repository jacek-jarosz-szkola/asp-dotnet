﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApplication.Database.Repositories
{
    interface IRepository<TEntity>
    {
        public TEntity GetById(int id);
    }
}
